---
menu: L’équipe
---

[g-team attributes="id:_team,class:team module"]

## L’équipe
Utilisez le module `team` pour présenter vos collaborateurs, partenaires, employés, etc…

___

[g-team-item image="jane.jpg" attributes="class:col-md-4"]
Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
<div class="item-social">
[g-link url="#" icon="asterisk" icon_type="fontawesome" stacked=true][/g-link]
[g-link url="#" icon="retweet" icon_type="fontawesome" stacked=true][/g-link]
[g-link url="#" icon="envelope" icon_type="fontawesome" stacked=true][/g-link]
</div>

[/g-team-item]

[g-team-item image="mark.jpg" attributes="class:col-md-4"]
Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
<div class="item-social">
[g-link url="#" icon="asterisk" icon_type="fontawesome" stacked=true][/g-link]
[g-link url="#" icon="retweet" icon_type="fontawesome" stacked=true][/g-link]
[g-link url="#" icon="envelope" icon_type="fontawesome" stacked=true][/g-link]
</div>

[/g-team-item]

[g-team-item image="julia.jpg" attributes="class:col-md-4"]
Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
<div class="item-social">
[g-link url="#" icon="asterisk" icon_type="fontawesome" stacked=true][/g-link]
[g-link url="#" icon="retweet" icon_type="fontawesome" stacked=true][/g-link]
[g-link url="#" icon="envelope" icon_type="fontawesome" stacked=true][/g-link]
</div>

[/g-team-item]
[/g-team]