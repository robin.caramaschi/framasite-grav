---
cache_enable: false
visible: false
---

[g-navbar id="navbar1" name=navbar1 fixed=top centering=none brand_text="Framasite" render=false]
    [g-navbar-menu name=menu0 alignment="center" submenu="autre" attributes="class:highdensity-menu"][/g-navbar-menu]    
    [g-navbar-menu name=menu1 icon_type="fontawesome" alignment="right" ]
        [g-link url="https://framasphere.org/u/framasoft" icon_type="fontawesome" icon="asterisk"][/g-link]
        [g-link url="https://framapiaf.org/@Framasoft" icon="retweet"][/g-link]
        [g-link url="https://framagit.org/framasoft/framasite" icon="git"][/g-link]
    [/g-navbar-menu]
[/g-navbar]

[g-footer-one name="footer" render=false]
[g-section name="credits"]

Ce site est motorisé par [Grav CMS](http://getgrav.org/) avec le theme [Gravstrap](http://diblas.net/themes/gravstrap-theme-to-start-grav-cms-site-with-bootstrap-support/) adapté par [Framasoft](https://framasoft.org/)

[/g-section]
[/g-footer-one]