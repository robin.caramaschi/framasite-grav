---
title: Carousel
menu: Top
fullwidth: true
slideme:
    id: content-slide
    arrows: 'true'
    autoslide: 'false'
    autoslideHoverStop: 'false'
    interval: '2000'
    loop: 'false'
    transition: zoom
    itemsForSlide: '0'
    touch: 'true'
    swipe: 'true'
slides:
    -
        image: image1.jpg
        heading: 'Titre 1 - Le projet'
        subheading: 'Texte pour inviter à lire le projet'
        button_text: 'LE PROJET'
        button_url: '#_what_we_do'
    -
        image: image2.jpg
        heading: 'Titre 2 - Porfolio'
        subheading: 'Texte pour inviter à consulter la galerie'
        button_text: PORTFOLIO
        button_url: '#_portfolio'
    -
        image: image3.jpg
        heading: 'Titre 3 - Clients'
        subheading: 'Texte pour en savoir plus'
        button_text: 'EN SAVOIR PLUS'
        button_url: '#_clients'
    -
        image: image4.jpg
        heading: 'Titre 3 - Contact'
        subheading: 'Texte pour invter à remplir le formulaire de contact'
        button_text: CONTACT
        button_url: ./contact
---

