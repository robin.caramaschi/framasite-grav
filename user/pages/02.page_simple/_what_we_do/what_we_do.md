---
menu: 'Le projet'
---

[g-what-we-do name="what_we_do" attributes="class:what-we-do module" column_attributes="id:_what_we_do,class:col-md-12"]

## Le projet
Utilisez le module `what_we_do` pour expliquer à vos visiteurs votre projet, les activités de votre entreprise, les missions de votre association, etc…
___

[g-what-we-do-item attributes="class:col-md-4"]

<div class="item-icon">
[g-icon icon="bullhorn fa-5x" icon_type="fontawesome"][/g-icon]
</div>
Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.

[/g-what-we-do-item]

[g-what-we-do-item attributes="class:col-md-4"]
<div class="item-icon">
[g-icon icon="bolt fa-5x" icon_type="fontawesome"][/g-icon]
</div>
Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.

[/g-what-we-do-item]

[g-what-we-do-item attributes="class:col-md-4"]

<div class="item-icon">
[g-icon icon="heart fa-5x" icon_type="fontawesome"][/g-icon]
</div>
Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
[/g-what-we-do-item]

[/g-what-we-do]