# v1.0.1
##  04/08/2017

1. [](#bugfix)
    * Problem solved with `NULL` on the `header()` function
     in the `onOutputGenerated` event


# v1.0.0
##  04/03/2017

1. [](#new)
    * Initial version committed to Github
