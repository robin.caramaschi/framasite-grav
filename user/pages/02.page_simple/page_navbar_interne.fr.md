---
title: 'Page simple'
metadata:
    description: 'Page d’exemple pour facilement composer une landing page avec plusieurs modules.'
slug: page-simple
content:
    items: '@self.modular'
    order:
        by: default
        dir: asc
        custom:
            - _header
            - _what_we_do
            - _portfolio
            - _clients
            - _team
---

