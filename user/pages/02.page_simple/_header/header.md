---
visible: false
cache_enable: false
---

[g-navbar id="navbar2" name=navbar2 fixed=top centering=none brand_text="Framasite" render=false]
    [g-navbar-menu name=menu0 alignment="center" onepage=true attributes="class:highdensity-menu"][/g-navbar-menu]
    [g-navbar-menu name=menu1 icon_type="fontawesome" alignment="right" attributes="class:social-menu"]
        [g-link url="https://framasphere.org/u/framasoft" icon_type="fontawesome" icon="asterisk"][/g-link]
        [g-link url="https://framapiaf.org/@Framasoft" icon="retweet"][/g-link]
        [g-link url="https://framagit.org/framasoft/framasite" icon="git"][/g-link]
    [/g-navbar-menu]    
[/g-navbar]

[g-jumbotron name="jumbotron1" fullwidth="true" image="bg_home.jpg" render=false]
# Simple page d’exemple

Voici une page d’exemple constituée des modules `header`, `what_we_do`, `portfolio`, `clients`, `team` avec une barre de navigation dont les liens permettent de faire défiler la page à chaque section.
[/g-jumbotron]
