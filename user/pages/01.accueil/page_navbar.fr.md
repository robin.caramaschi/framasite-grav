---
title: Accueil
slug: accueil
editable-simplemde:
    self: false
---

[g-jumbotron name="jumbotron1" fullwidth="true" image="bg_cat.jpg" render=false]
# Bienvenue sur votre Framasite

[/g-jumbotron]

[g-what-we-do name="what_we_do" attributes="class:what-we-do module" column_attributes="class:col-md-12"]

[g-what-we-do-item attributes="class:col-md-12"]
<div class="item-icon">
[g-icon icon="cut fa-5x" icon_type="fontawesome"][/g-icon]
</div>
Les Framasites ne sont proposés qu’avec un seul thème par défaut
mais il permet une certaine souplesse quant au type de site que vous voulez créer.

Les pages que vous pouvez consulter ici en démonstration illustrent l’ensemble des possibilités.
Vous disposez de 6 pages d’exemples :

[g-list attributes="class:text-left"]
[g-list-item]Cette page d’accueil qui affiche une barre de navigation et un pied de page pour un accès rapide aux pages principales du site[/g-list-item]
[g-list-item]Une [page simple](./page-simple) avec plusieurs modules et une barre de navigation dont les liens pointent vers ses différentes sections[/g-list-item]
[g-list-item]Une [page avec un carrousel](./page-avec-carrousel) en entête et les mêmes modules que la page « simple »[/g-list-item]
[g-list-item]Un [blog](./blog) dont les articles sont listés et classés automatiquement
par date de publication. Une colonne à droite permet de naviguer parmi les articles.[/g-list-item]
[g-list-item]Un [formulaire de contact](./contact)[/g-list-item]
[g-list-item]Une [page de présentatior des auteur·e·s](./a-propos) du site. Elle peut aussi servir à présenter un CV professionnel.[/g-list-item]
[/g-list]

Chacune de ces pages peut servir de page d’accueil. 
Des modèles génériques (menu « Autre » correspondant au template `internal`) peuvent aussi vous aider à structurer vos pages en plusieurs colonnes.

Une fois que vous saurez exactement comment vous voulez construire votre site,
nous vous invitons à dépublier ces pages.

Le thème repose sur le [framework](https://fr.wikipedia.org/wiki/Framework)
[Bootstrap 3.3](https://getbootstrap.com/docs/3.3/), si bien que vous
pouvez agencer, habiller et personnaliser votre site en utilisant certains de
ses composants.

N’hésitez pas à consulter la [documentation](https://docs.framasoft.org/fr/grav/).

La rédaction des pages se fait avec la syntaxe [markdown](https://docs.framasoft.org/fr/grav/markdown.html).
Il est possible d’utiliser du code html ou bien une syntaxe simplifiée pour insérer des composants ou utiliser des modules de présentation.

[g-list attributes="class:text-left"]
[g-list-item][Composants de base](https://docs.framasoft.org/fr/grav/composants-de-base.html)[/g-list-item]
[g-list-item][Composants Bootstrap](https://docs.framasoft.org/fr/grav/composants-bootstrap.html)[/g-list-item]
[g-list-item][Modules](https://docs.framasoft.org/fr/grav/modules.html)[/g-list-item]
[/g-list]

[/g-what-we-do-item]

[/g-what-we-do]
